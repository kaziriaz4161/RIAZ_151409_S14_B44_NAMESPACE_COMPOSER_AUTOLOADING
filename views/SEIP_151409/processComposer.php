<?php


require_once("../../vendor/autoload.php");

use App\Person;
use App\Student;

if(empty($_POST["StudentID"])){
    echo "I'm a person <br>";

    $objPerson  = new Person();

    $objPerson -> setName($_POST["Name"]);
    $objPerson -> setDateOfBirth($_POST["DOB"]);

    echo $objPerson->getName() ."<br>";
    echo $objPerson->getDateOfBirth() ."<br>";

}
else {
    echo "I'm a student<br>";
    $objStudent = new Student();

    $objStudent->setName($_POST["Name"]);
    $objStudent->setStudentID($_POST["StudentID"]);
    $objStudent->setDateOfBirth($_POST["DOB"]);

    echo $objStudent->getName() ."<br>";
    echo $objStudent->getStudentID() ."<br>";
    echo $objStudent->getDateOfBirth() ."<br>";

}